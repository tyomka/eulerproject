package com.fatunicorn.solver;

import com.fatunicorn.core.SolverTerminationCallback;

/**
 * Abstract implementation of a Solver
 *
 * Created by Artem Chernyshov on 07.05.2017.
 */
public class AbstractSolver implements SolverApplicationAware {

    private SolverTerminationCallback callback;

    AbstractSolver(SolverTerminationCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onTermination() {
        callback.returnToCommandline();
    }
}
