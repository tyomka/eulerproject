package com.fatunicorn.solver;

/**
 * Notifies {@see SolverApplication} of occuring events
 *
 * Created by Artem Chernyshov on 10.05.2017.
 */
public interface SolverApplicationAware {

    void onTermination();
}
