package com.fatunicorn.ui;

/**
 * Interface declaring commandline functionality
 *
 * Created by Artem Chernyshov on 10.05.2017.
 */
public interface CommandlineBased {

    void startCommandline();
}
