package com.fatunicorn.ui;

import java.util.Scanner;

/**
 * Implementation of the main commandline
 *
 * Created by Artem Chernyshov on 07.05.2017.
 */
public class MainCommandline implements CommandlineBased {

    @Override
    public void startCommandline() {
        System.out.println("----- Euler Project Solutions -----\n");
        System.out.println("Please enter the number of an Euler problem: ");
        Scanner reader = new Scanner(System.in);
        int problemNumber = reader.nextInt();


    }
}
