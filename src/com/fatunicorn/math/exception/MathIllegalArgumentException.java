package com.fatunicorn.math.exception;

/**
 * Created by Artem Chernyshov on 10.05.2017.
 */
public class MathIllegalArgumentException extends Throwable {
    public MathIllegalArgumentException(String s) {
        super(s);
    }
}
