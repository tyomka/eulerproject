package com.fatunicorn.math.primes;

import com.fatunicorn.math.exception.MathIllegalArgumentException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Artem Chernyshov on 10.05.2017.
 */
public class Primes {

    /**
     * * Prime factors decomposition
     *
     * @param inValue number to decompose into prime factors
     * @return set of prime factors
     */
    public static Set<Long> primeFactors(Long inValue) throws MathIllegalArgumentException {

        if(inValue < 2) {
            throw new MathIllegalArgumentException("Number must be greater than one.");
        }

        Set<Long> primeFactors = new HashSet<>();

        while(inValue % 2 == 0) {
            primeFactors.add(2L);
            inValue = inValue / 2;
        }

        for(long i = 3; i < Math.sqrt(inValue); i=i+2) {
            while(inValue % i == 0) {
                primeFactors.add(i);
                inValue = inValue / i;
            }
        }

        if(inValue > 2) {
            primeFactors.add(inValue);
        }
        return primeFactors;
    }
}
