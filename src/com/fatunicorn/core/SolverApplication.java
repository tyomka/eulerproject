package com.fatunicorn.core;

import com.fatunicorn.ui.MainCommandline;
import com.fatunicorn.ui.CommandlineBased;

/**
 * Main application managing commandline and solver execution.
 *
 * Created by Artem Chernyshov on 10.05.2017.
 */
public class SolverApplication implements SolverTerminationCallback {

    private CommandlineBased menu ;

    @Override
    public void returnToCommandline() {
        executeCommandline();
    }

    public void executeCommandline() {
        if(menu == null) {
            menu = new MainCommandline();
        }
        menu.startCommandline();
    }
}
