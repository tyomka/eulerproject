package com.fatunicorn.core;

/**
 * Callback interface aware of solver termination
 *
 * Created by Artem Chernyshov on 10.05.2017.
 */
public interface SolverTerminationCallback {

    void returnToCommandline();
}
