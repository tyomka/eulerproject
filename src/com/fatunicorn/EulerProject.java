package com.fatunicorn;

import com.fatunicorn.core.SolverApplication;

/**
 * Solution choser ui implemenation
 *
 * Created by Artem Chernyshov on 07.05.2017.
 */
public class EulerProject {

    public static void main(String[] args) {
        SolverApplication solverApplication = new SolverApplication();
        solverApplication.executeCommandline();
    }
}
